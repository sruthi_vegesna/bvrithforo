<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>List Of Events</title>
</head>
<body>
<table border="2">
<c:forEach var = "event" items="${eventsList}">

<tr>
<td>${event.ename}</td>
<td>${event.ewhen}</td>
<td>${event.ewhere}</td>
<td>${event.edetails}</td>
<td>${event.access}</td>
</tr>


</c:forEach>
</table>
</body>
</html>