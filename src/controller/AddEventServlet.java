package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.AddEventDao;
import dto.AddEventDto;
@WebServlet("/AddEventServlet")
public class AddEventServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
    public AddEventServlet() {
        super();
    }
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 PrintWriter out=response.getWriter();
         response.setContentType("text/html");
         String ename = request.getParameter("ename");
         String ewhen = request.getParameter("ewhen");
         String ewhere=request.getParameter("ewhere");
         String edetails=request.getParameter("edetails");
         String access=request.getParameter("access");
         System.out.println(access);
         
         String localAccess="",localEname="",localEwhen="",localEwhere="",localEdetails="";
 		System.out.println(ename+" "+access);
         
         AddEventDto q= new AddEventDto();
         
         q.setEname(ename);
         q.setEwhen(ewhen);
         q.setEwhere(ewhere);
         q.setEdetails(edetails);
         q.setAccess(access);
        
        String url=null;
 		boolean uss=dao.AddEventDao.insertEvent(q);
 		System.out.println("getOne Returned"+uss);
 		out.println("<h3 style = 'color:red'>Event Inserted</h3>");
		RequestDispatcher rd = request.getRequestDispatcher("/campus.html");
		rd.forward(request,response);
}
}
