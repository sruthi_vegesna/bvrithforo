package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.AddEventDao;
import dto.AddEventDto;


/**
 * Servlet implementation class GetAllEvents
 */
@WebServlet("/GetAllEvents")
public class GetAllEvents extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetAllEvents() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			
			AddEventDao addeventdao = new AddEventDao();
			List<AddEventDto> list = addeventdao.getAll();
			
			request.setAttribute("eventsList", list);
			request.getRequestDispatcher("listEvents.jsp").forward(request, response);
			
		}
}
