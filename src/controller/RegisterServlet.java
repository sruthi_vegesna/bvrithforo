package controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dto.RegisterDto;
import dao.RegisterDao;


@WebServlet("/RegisterServlet")
public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
      
    public RegisterServlet() {
        super();
        
    }
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 PrintWriter out=response.getWriter();
         response.setContentType("text/html");
         String stdid = request.getParameter("stdid");
         String name = request.getParameter("name");
         int passout =Integer.parseInt(request.getParameter("passout"));
         String branch=request.getParameter("branch");
         String email=request.getParameter("email");
         String password=request.getParameter("password");
         RegisterDto q= new RegisterDto();
         
         q.setStdid(stdid);
         q.setName(name);
         q.setPassout(passout);
         q.setBranch(branch);
         q.setEmail(email);
         q.setPassword(password);
         //q.setSecurityq(Securityq);
        // q.setSecurityans(Securityans);
         boolean b = RegisterDao.insertRegister(q);
         if (b == true) {
             RequestDispatcher rd = request.getRequestDispatcher("login.html");
             rd.forward(request, response);
     }
         else {
             out.println("record not found");
            
         }
      
}
}

