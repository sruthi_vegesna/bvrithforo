package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dto.LoginDto;
import dao.LoginDao;

/**
 * Servlet implementation class loginServlet
 */
@WebServlet("/loginServlet")
public class loginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public loginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out =  response.getWriter();
		response.setContentType("text/html");
		String Email = request.getParameter("email");
		String Password = request.getParameter("password");
		String Role=request.getParameter("role");
		
		System.out.println(Role);
		
		String localRole="",localU="",localP="";
		System.out.println(Email+" "+Role);
		
		LoginDto e= new LoginDto();
		e.setUsername(Email);
		e.setPassword(Password);
		e.setRole(Role);
		String url=null;
		ArrayList<LoginDto> uss=LoginDao.getOne(e);
		for(LoginDto usa:uss)
		{
			localRole=usa.getRole();
			localU=usa.getUsername();
			localP=usa.getPassword();
		}
		
			if(localRole.equals("admin"))
			{	
				System.out.println("Admin");
				HttpSession sess=request.getSession();
				sess.setAttribute("Atdname", Email);
			RequestDispatcher rd = request.getRequestDispatcher("/admin.html");
			rd.forward(request,response);
			}
			else if(localRole.equals("student"))
			{
				HttpSession sess=request.getSession();
				sess.setAttribute("Stdname", Email);
				RequestDispatcher rd = request.getRequestDispatcher("/studentHome.jsp");
				rd.forward(request,response);
			}
			else if(localRole.equals("alumni"))
			{
				HttpSession sess=request.getSession();
				sess.setAttribute("Alumname", Email);
				RequestDispatcher rd = request.getRequestDispatcher("/alumniHome.jsp");
				rd.forward(request,response);
				
		}
				
	
	else{
		out.println("<h3 style = 'color:red'>record not found</h3>");
		RequestDispatcher rd = request.getRequestDispatcher("/campus.html");
	}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
