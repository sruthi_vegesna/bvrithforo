package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import connection.DatabaseConnection;
import dto.AddEventDto;
import dto.LoginDto;

public class AddEventDao {
	public static boolean insertEvent(AddEventDto q) {
        try
        {
            Connection con = DatabaseConnection.getConnection();
            PreparedStatement ps = con.prepareStatement("insert into event values(?,?,?,?,?)");
            ps.setString(1, q.getEname());
            ps.setString(2,q.getEwhen());
            ps.setString(3,q.getEwhere());
            ps.setString(4, q.getEdetails());
            ps.setString(5, q.getAccess());
            ps.executeUpdate();
            return true;
        }
        catch(Exception ls){
            System.out.println(ls);
        }
        return false;
       
                   
        }
    public static ArrayList<AddEventDto> getAll() {
        ArrayList<AddEventDto> al= new ArrayList<AddEventDto>();
        ResultSet rs = null;
        try
        {
            Connection con = DatabaseConnection.getConnection();
            PreparedStatement  ps = con.prepareStatement("select * from event");
            rs = ps.executeQuery();
   // Statement st =con.createStatement();
    //ResultSet rs= st.executeQuery("select * from event");
    while(rs.next())
    {
    	AddEventDto u=new AddEventDto();
    	
    u.setEname(rs.getString(1));
    u.setEwhen(rs.getString(2));
    u.setEwhere(rs.getString(3));
    u.setEdetails(rs.getString(4));
    u.setAccess(rs.getString(5));
    al.add(u);
        }
    System.out.println("THis is "+al);
        
    } catch(Exception es) { 
    	System.out.println(es);
    	}
        return al;
	/*public static ArrayList<AddEventDto> getOne(AddEventDto e) {
		System.out.println(e.getEname()+" "+e.getEwhen()+" "+e.getEwhere()+" "+e.getEdetails()+" "+e.getAccess());
	ArrayList<AddEventDto> alp=new ArrayList<AddEventDto>();
	try
	{
		Connection con=DatabaseConnection.getConnection();
		PreparedStatement ps=con.prepareStatement("Select * from event where ename = ? and ewhen = ? and ewhere = ? and edetails = ? and access = ?");
		ps.setString(1, e.getEname());
		ps.setString(2, e.getEwhen());
		ps.setString(3,e.getEwhere());
		ps.setString(4,e.getEdetails());
		ps.setString(5,e.getAccess());
		System.out.println("Query="+ps);
		ResultSet rs=ps.executeQuery();
		while(rs.next())
		{
			AddEventDto lp=new AddEventDto();
			lp.setEname(rs.getString(1));
			lp.setEwhen(rs.getString(2));
			lp.setEwhere(rs.getString(3));
			lp.setEdetails(rs.getString(4));
			lp.setAccess(rs.getString(5));
			alp.add(lp);
		}
		//System.out.println("This is"+alp);
		return alp;
	}
	catch(Exception eq){System.out.println(eq);}*/
    
    }
}
