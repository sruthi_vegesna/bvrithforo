package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import dto.LoginDto;
import connection.DatabaseConnection;

public class LoginDao {
	public static boolean insertLoginDto(LoginDto e) {
		try {
			Connection con = DatabaseConnection.getConnection();
			PreparedStatement ps = con.prepareStatement("insert into login values(?,?,?)");
			ps.setString(1, e.getUsername());
			ps.setString(2, e.getPassword());
			ps.setString(3, e.getRole());
			ps.executeUpdate();
			return true;
		}
		catch(Exception es) {
		System.out.println(es);
		}
		return false;
	}

	public static ArrayList<LoginDto> getOne(LoginDto e) {
		// TODO Auto-generated method stub
		System.out.println(e.getUsername()+" "+e.getPassword()+" "+e.getRole());
	ArrayList<LoginDto> alp=new ArrayList<LoginDto>();
	try
	{
		Connection con=DatabaseConnection.getConnection();
		PreparedStatement ps=con.prepareStatement("Select * from login where email = ? and password = ? and role = ?");
		ps.setString(1, e.getUsername());
		ps.setString(2, e.getPassword());
		ps.setString(3,e.getRole());
		ResultSet rs=ps.executeQuery();
		while(rs.next())
		{
			LoginDto lp=new LoginDto();
			lp.setUsername(rs.getString("email"));
			lp.setPassword(rs.getString("password"));
			lp.setRole(rs.getString("role"));
			alp.add(lp);
		}
		System.out.println("This is"+alp);
		return alp;
	}
	catch(Exception eq){System.out.println(eq);}
	return null;
} 
}
