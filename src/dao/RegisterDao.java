package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import dto.RegisterDto;
import connection.DatabaseConnection;

public class RegisterDao {
	public static boolean insertRegister(RegisterDto q) {
        try
        {
            Connection con = DatabaseConnection.getConnection();
            PreparedStatement ps = con.prepareStatement("insert into registration values(?,?,?,?,?,?)");
            ps.setString(1, q.getStdid());
            ps.setString(2,q.getName());
            ps.setInt(3,q.getPassout());
            ps.setString(4, q.getBranch());
            ps.setString(5, q.getEmail());
            ps.setString(6, q.getPassword());
            ps.executeUpdate();
            return true;
        }
        catch(Exception ls){
            System.out.println(ls);
        }
        return false;
       
                   
        }
    public static ArrayList<RegisterDto> getAll() {
        ArrayList<RegisterDto> al= new ArrayList<RegisterDto>();
        try
        {
            Connection con = DatabaseConnection.getConnection();
    Statement st =con.createStatement();
    ResultSet rs= st.executeQuery("select * from registration");
    while(rs.next())
    {
    	RegisterDto u=new RegisterDto();
    	
    u.setStdid(rs.getString("stdid"));
    u.setName(rs.getString("name"));
    u.setPassout(rs.getInt("passout"));
    u.setBranch(rs.getString("branch"));
    u.setEmail(rs.getString("email"));
    u.setPassword(rs.getString("password"));
    al.add(u);
        }
        return al;
    }
catch(Exception es){ System.out.println(es);}
return null;
}
}
