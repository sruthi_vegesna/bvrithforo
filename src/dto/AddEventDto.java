package dto;

public class AddEventDto {
	private String ename;
	private String ewhen;
	private String ewhere;
	private String edetails;
	private String access;
	public String getEname() {
		return ename;
	}
	public void setEname(String ename) {
		this.ename = ename;
	}
	public String getEwhen() {
		return ewhen;
	}
	public void setEwhen(String ewhen) {
		this.ewhen = ewhen;
	}
	public String getEwhere() {
		return ewhere;
	}
	public void setEwhere(String ewhere) {
		this.ewhere = ewhere;
	}
	public String getEdetails() {
		return edetails;
	}
	public void setEdetails(String edetails) {
		this.edetails = edetails;
	}
	public String getAccess() {
		return access;
	}
	public void setAccess(String access) {
		this.access = access;
	}
	
}
